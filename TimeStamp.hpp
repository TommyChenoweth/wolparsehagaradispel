#pragma once
#include <iostream>
#include <string>

class TimeStamp
{
private:
	unsigned _hours, _minutes, _seconds, _milliseconds;

	bool _TimeStampStringFormattedCorrectly(const std::string& timestamp);

public:
	explicit TimeStamp(const std::string& timestamp);
	TimeStamp();

	unsigned GetHours() const;
	unsigned GetMinutes() const;
	unsigned GetSeconds() const;
	unsigned GetMilliseconds() const;

	bool operator==(const TimeStamp& other);
	friend std::ostream& operator<<(std::ostream& os, const TimeStamp& timestamp);
};