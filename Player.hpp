#pragma once
#include <string>
#include "TimeStamp.hpp"

class Player
{
private:
	bool has_watery_entrenchment;

	bool has_frostflake;
	TimeStamp time_frostflake_removed;

	int num_times_fd_up;

public:
	Player();

	void ApplyWateryEntrenchment();
	void RemoveWateryEntrenchment();
	bool HasWateryEntrenchment() const;

	void ApplyFrostflake();
	void RemoveFrostflake(const TimeStamp& time_of_removal);
	bool HasFrostflake() const;
	TimeStamp GetTimeOfFrostflakeRemoval() const;

	void AddFUp();
	int GetNumTimesFdUp() const;
};