#include <regex>
#include <stack>
#include "TimeStamp.hpp"

bool TimeStamp::_TimeStampStringFormattedCorrectly(const std::string& timestamp)
{
	const static std::regex TIMESTAMP_PATTERN("[\[][0-9]{2}[:][0-9]{2}[:][0-9]{2}[.][0-9]{3}[\\]]");
	return std::regex_match(timestamp, TIMESTAMP_PATTERN);
}

TimeStamp::TimeStamp(const std::string& timestamp)
	: _hours(0),
	  _minutes(0),
	  _seconds(0),
	  _milliseconds(0)
{
	if(!_TimeStampStringFormattedCorrectly(timestamp))
		throw std::exception("TimeStamp construction failed: Bad string passed to constructor.");

	_hours += (timestamp[1]-'0')*10;
	_hours += (timestamp[2]-'0');

	_minutes += (timestamp[4]-'0')*10;
	_minutes += (timestamp[5]-'0');

	_seconds += (timestamp[7]-'0')*10;
	_seconds += (timestamp[8]-'0');

	_milliseconds += (timestamp[10]-'0')*100;
	_milliseconds += (timestamp[11]-'0')*10;
	_milliseconds += (timestamp[12]-'0');
}

TimeStamp::TimeStamp()
	: _hours(0),
	  _minutes(0),
	  _seconds(0),
	  _milliseconds(0)
{
}

unsigned TimeStamp::GetHours() const
{
	return _hours;
}

unsigned TimeStamp::GetMinutes() const
{
	return _minutes;
}

unsigned TimeStamp::GetSeconds() const
{
	return _seconds;
}

unsigned TimeStamp::GetMilliseconds() const
{
	return _milliseconds;
}

bool TimeStamp::operator==(const TimeStamp& other)
{
	return (_hours == other.GetHours()
		&& _minutes == other.GetMinutes()
		&& _seconds == other.GetSeconds()
		&& _milliseconds == other.GetMilliseconds());
}

std::ostream& operator<<(std::ostream& os, const TimeStamp& timestamp)
{
	return os << "[" << timestamp.GetHours() << ":" << timestamp.GetMinutes() << ":" << timestamp.GetSeconds() << "." << timestamp.GetMilliseconds() << "]";
}