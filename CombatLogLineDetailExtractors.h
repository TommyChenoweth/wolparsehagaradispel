#pragma once

#include <string>

// Get the source name from a line in a combat-log.
std::string ExtractSourceName(const std::string& line);

// Get the target name from a line in a combat-log.
std::string ExtractTargetName(const std::string& line);

// Get a combat-log line's timestamp.
std::string ExtractTimeStamp(const std::string& line);

// Get the target of a dispel from a line in a combat-log.
std::string ExtractDispelTarget(const std::string& line);

std::string Death_ExtractTargetName(const std::string& line);