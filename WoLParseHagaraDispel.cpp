#include <algorithm>
#include "CombatLogLineDetailExtractors.h"
#include <exception>
#include <fstream>
#include <functional>
#include <iostream>
#include "Player.hpp"
#include <string>
#include <unordered_map>
#include <vector>

typedef std::unordered_map<std::string, Player> NameToPlayerMap;

typedef std::pair<std::string, std::function<void(const std::string&, NameToPlayerMap&)>> LineIdentifierAndAction;
typedef std::vector<LineIdentifierAndAction> LineIdentifierAndActionVector;

void ParseCombatLog(const std::string& filename);
void ParseLine(std::string line, NameToPlayerMap& name_to_player, LineIdentifierAndActionVector& line_types_and_actions);

bool LineContainsString(const std::string& line, const std::string& string);

// Print, to the console, a list of players who caused ice patches to drop on the
// ground along with the number of occurrences.
void OutputPlayerFUps(const NameToPlayerMap& name_to_player);

void InitializeLineIdentifiersAndActions(LineIdentifierAndActionVector& line_types_and_actions);

int main(int argc, char* argv[])
{
	// Ensure that the combat-log filename is passed in as an argument.
	if(argc != 2)
	{
		std::cout << "Invalid number of arguments. Pass in the name of the combat-log.\n";
		return 0;
	}

	const std::string FILENAME(argv[1]);
	ParseCombatLog(FILENAME);

	#ifdef _DEBUG
	system("PAUSE");
	#endif

	return 0;
}

void ParseCombatLog(const std::string& filename)
{
	// name_to_player maps a player's name to a Player object.
	NameToPlayerMap name_to_player;

	// This vector is used to match types of combat-log lines to functions. The first value of each element
	// is a string that is characteristic of a certain type of combat-log line. The second value of each
	// element is a function that should be executed for that type of combat-log line.
	LineIdentifierAndActionVector line_types_and_actions;

	InitializeLineIdentifiersAndActions(line_types_and_actions);

	// There are 10 players in a 10 man raid, so we should only need 10 buckets to achieve
	// constant access time in a hashmap.
	static const int NUMBER_OF_PLAYERS = 10;
	name_to_player.rehash(NUMBER_OF_PLAYERS);

	std::ifstream cl_in;
	try
	{
		cl_in.open(filename.c_str(), std::ifstream::binary);

		std::vector<char> buffer(120, '_');

		while(!cl_in.eof() && cl_in.is_open())
		{
			cl_in.getline(&buffer[0], buffer.size());

			if(cl_in.fail())
			{
				buffer.resize(buffer.size()*2);
				cl_in.clear();

				std::cerr << "The file input buffer was too small to read a complete line. Resized to " << buffer.size() << std::endl;

				// Move the get pointer back to where it was before the read.
				cl_in.seekg(-cl_in.gcount(), std::fstream::cur);

				continue;
			}

			ParseLine(&buffer[0], name_to_player, line_types_and_actions);
		}

		OutputPlayerFUps(name_to_player);
	}
	catch(const std::exception& e)
	{
		// I don't really care about exceptions in this program because it will only be used
		// by me, and it's so small.
		std::cout << e.what() << std::endl;
	}
}

void ParseLine(std::string line, NameToPlayerMap& name_to_player, LineIdentifierAndActionVector& line_types_and_actions)
{
	auto lta = line_types_and_actions.begin();
	bool line_parsed(false);
	while(!line_parsed && lta != line_types_and_actions.end())
	{
		if(LineContainsString(line, lta->first))
		{
			lta->second(line, name_to_player);
			line_parsed = true;
		}
		++lta;
	}
}

bool LineContainsString(const std::string& line, const std::string& string)
{
	return (std::string::npos != line.find(string));
}

void OutputPlayerFUps(const NameToPlayerMap& name_to_player)
{
	std::cout << "\nThese players put ice patches on the ground: \n";
	std::for_each(name_to_player.begin(), name_to_player.end(),
		[](const NameToPlayerMap::value_type& p)
		{
			if(p.second.GetNumTimesFdUp() != 0) 
				std::cout << p.first << " f'd up " << p.second.GetNumTimesFdUp() << " time(s).\n";
		});
}

void InitializeLineIdentifiersAndActions(LineIdentifierAndActionVector& line_types_and_actions)
{
	line_types_and_actions.emplace_back(LineIdentifierAndAction("afflicted by Watery", 
		[](const std::string& line, NameToPlayerMap& ntp)
		{
			ntp[ExtractSourceName(line)].ApplyWateryEntrenchment();
		}));

	line_types_and_actions.emplace_back(LineIdentifierAndAction("Entrenchment fades",
		[](const std::string& line, NameToPlayerMap& ntp)
		{
			ntp[ExtractSourceName(line)].RemoveWateryEntrenchment();
		}));

	line_types_and_actions.emplace_back(LineIdentifierAndAction("afflicted by Frostflake",
		[](const std::string& line, NameToPlayerMap& ntp)
		{
			ntp[ExtractSourceName(line)].ApplyFrostflake();
		}));

	line_types_and_actions.emplace_back(LineIdentifierAndAction("Frostflake fades",
		[](const std::string& line, NameToPlayerMap& ntp)
		{
			std::string target_name = ExtractTargetName(line);
			ntp[target_name].RemoveFrostflake(TimeStamp(ExtractTimeStamp(line)));
			if(!ntp[target_name].HasWateryEntrenchment())
			{
					std::cout << target_name << " dropped Frostflake outside of the Watery Entrenchment @ " << ExtractTimeStamp(line) << std::endl;
			}
		}));

	line_types_and_actions.emplace_back(LineIdentifierAndAction("dispells",
		[](const std::string& line, NameToPlayerMap& ntp)
		{
			std::string source_name = ExtractSourceName(line);
			std::string target_name = ExtractDispelTarget(line);

			// Only Frostflake dispels show up in the combat log, and you cannot cast dispel on a player
			// without a debuff to dispel; this means we can assume that a player has Frostflake at
			// the time a dispel is applied to him - we don't need to check the player's Frostflake
			// status; furthermore, we should not check the Frostflake status because the dispel effect
			// may appear after the combat-log shows Frostflake falling off.
			if(!ntp[target_name].HasWateryEntrenchment())
			{
				ntp[source_name].AddFUp();
				std::cout << source_name << " dispelled Frostflake from " << target_name << std::endl;
			}
		}));

	auto snare_breaker_casted = [](const std::string& line, NameToPlayerMap& ntp)
	{
		std::string source_name = ExtractSourceName(line);

		bool player_with_frostflake_outside_of_watery_entrenchment(ntp[source_name].HasFrostflake() && !ntp[source_name].HasWateryEntrenchment());
		// Sometimes the snare breaker that broke Frostflake appears after Frostflake has fallen off.
		// In this case, we must compare timestamps to determine whether or not the snare breaker was 
		// casted at the same time that Frostflake fell off. If they occurred at the same time, we assume
		// that the snare breaker caused it.
		bool frostflake_removed_at_same_time_as_snare_breaker_casted(!ntp[source_name].HasWateryEntrenchment() && (ntp[source_name].GetTimeOfFrostflakeRemoval() == TimeStamp(ExtractTimeStamp(line))));

		if(player_with_frostflake_outside_of_watery_entrenchment || frostflake_removed_at_same_time_as_snare_breaker_casted)
		{
			ntp[source_name].AddFUp();
			std::cout << source_name << " casted a snare breaker while affected by Frostflake outside of the Watery Entrenchment!\n";
		}
	};

	line_types_and_actions.emplace_back(LineIdentifierAndAction("Ice Block", snare_breaker_casted));
	line_types_and_actions.emplace_back(LineIdentifierAndAction("Cat Form", snare_breaker_casted));
	line_types_and_actions.emplace_back(LineIdentifierAndAction("Bear Form", snare_breaker_casted));
	line_types_and_actions.emplace_back(LineIdentifierAndAction("Cloak of Shadows", snare_breaker_casted));
	line_types_and_actions.emplace_back(LineIdentifierAndAction("Stampeding Roar", snare_breaker_casted));
	line_types_and_actions.emplace_back(LineIdentifierAndAction("Dash", snare_breaker_casted));

	line_types_and_actions.emplace_back(LineIdentifierAndAction(" (O: ",
		[](const std::string& line, NameToPlayerMap& ntp)
		{
			std::string target_name = Death_ExtractTargetName(line);

			bool player_has_frostflake_outside_of_watery_entrenchment(ntp[target_name].HasFrostflake() && !ntp[target_name].HasWateryEntrenchment());
			// Sometimes a player's death will cause Frostflake to fall off, but the killing blow shows up 
			// after Frostflake falls off in the combat log. In this case, we must compare timestamps to 
			// determine whether the player's death caused Frostflake to fall off. If the killing blow
			// occurred at the same time that Frostflake fell off, we assume that the the death caused it.
			bool frostflake_removed_at_same_time_as_killing_blow(!ntp[target_name].HasWateryEntrenchment() && ntp[target_name].GetTimeOfFrostflakeRemoval() == TimeStamp(ExtractTimeStamp(line)));

			if(player_has_frostflake_outside_of_watery_entrenchment || frostflake_removed_at_same_time_as_killing_blow)
			{
				std::cout << target_name << " died with Frostflake outside of Watery Entrenchment!\n";
			}
		}));
}