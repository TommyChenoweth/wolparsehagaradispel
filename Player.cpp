#include "Player.hpp"
Player::Player()
	: has_watery_entrenchment(false),
	  has_frostflake(false),
	  num_times_fd_up(0)
{
}

void Player::ApplyWateryEntrenchment()
{
	has_watery_entrenchment = true;
}

void Player::RemoveWateryEntrenchment()
{
	has_watery_entrenchment = false;
}

bool Player::HasWateryEntrenchment() const
{
	return has_watery_entrenchment;
}

void Player::ApplyFrostflake()
{
	has_frostflake = true;
}

void Player::RemoveFrostflake(const TimeStamp& time_of_removal)
{
	has_frostflake = false;
	time_frostflake_removed = time_of_removal;
}

bool Player::HasFrostflake() const
{
	return has_frostflake;
}

TimeStamp Player::GetTimeOfFrostflakeRemoval() const
{
	return time_frostflake_removed;
}

void Player::AddFUp()
{
	++num_times_fd_up;
}
int Player::GetNumTimesFdUp() const
{
	return num_times_fd_up;
}