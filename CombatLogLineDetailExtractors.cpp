#include "CombatLogLineDetailExtractors.h"
#include <regex>
#include <string>

std::string ExtractSourceName(const std::string& line)
{
	const static std::regex SOURCE_NAME_PATTERN("[[:alpha:]]+");
	std::smatch source_name_match;
	std::regex_search(line, source_name_match, SOURCE_NAME_PATTERN);
	return source_name_match.str();
}

std::string ExtractTargetName(const std::string& line)
{
	const static std::regex TARGET_NAME_PATTERN("([[:alpha:]]+)\\r");
	std::smatch target_name_smatch;
	std::regex_search(line, target_name_smatch, TARGET_NAME_PATTERN);
	return target_name_smatch[1].str();
}

std::string ExtractTimeStamp(const std::string& line)
{
	const static std::regex TIMESTAMP_PATTERN("[\[].+[\\]]");
	std::smatch timestamp_smatch;
	std::regex_search(line, timestamp_smatch, TIMESTAMP_PATTERN);
	return timestamp_smatch.str();
}

std::string ExtractDispelTarget(const std::string& line)
{
	const static std::regex DISPEL_TARGET_PATTERN("[^\\]][[:space:]]([[:alpha:]]+)[']");
	std::smatch dispel_target_smatch;
	std::regex_search(line, dispel_target_smatch, DISPEL_TARGET_PATTERN);
	return dispel_target_smatch[1].str();
}

std::string Death_ExtractTargetName(const std::string& line)
{
	const static std::regex TARGET_NAME_PATTERN("([[:alpha:]]+)[[:space:]][0-9]");
	std::smatch target_name_smatch;
	std::regex_search(line, target_name_smatch, TARGET_NAME_PATTERN);
	return target_name_smatch[1].str();
}